<?php

namespace App\Controllers;

use App\Models\HardwareModel;
use App\Models\SoftwareModel;
use App\Models\ProdukModel;
use App\Models\IpModel;


class Inventory extends BaseController
{

    protected $InventoryModel;

    function __construct()
    {
        $this->hardware = new HardwareModel();
        $this->software = new SoftwareModel();
        $this->produk = new ProdukModel();
        $this->ip = new IpModel();
    }

    public function index()
    {
        $data['hardware'] = $this->hardware->findAll();
        $data['software'] = $this->software->findAll();
        $data['produk'] = $this->produk->findAll();
        $data['ip'] = $this->ip->findAll();
        return view('inventory', $data);
    }
}

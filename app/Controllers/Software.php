<?php

namespace App\Controllers;

use App\Models\SoftwareModel;

class Software extends BaseController
{
    protected $software;

    function __construct()
    {
        $this->software = new SoftwareModel();
    }

    public function index()
    {
        $data['software'] = $this->software->findAll();
        return view('/software/index', $data);
    }

    public function create()
    {
        return view('software/create');
    }

    public function save()
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'jumlah' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'status' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $this->software->insert([
            'nama' => $this->request->getVar('nama'),
            'jumlah' => $this->request->getVar('jumlah'),
            'status' => $this->request->getVar('status'),
        ]);
        session()->setFlashdata('message', 'Tambah Data software Berhasil');
        return redirect()->to('/software');
    }

    public function edit($id)
    {
        $dataSoftware = $this->software->find($id);
        if (empty($dataSoftware)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data software tidak ditemukan !');
        }
        $data['software'] = $dataSoftware;
        return view('software/edit', $data);
    }

    public function update($id)
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'jumlah' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'status' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back();
        }

        $this->software->update($id, [
            'nama' => $this->request->getVar('nama'),
            'jumlah' => $this->request->getVar('jumlah'),
            'status' => $this->request->getVar('status'),
        ]);
        session()->setFlashdata('message', 'Update Data software berhasil');
        return redirect()->to('/software');
    }

    public function delete($id)
    {
        $this->software->delete($id);
        return redirect()->to('/software');
    }
}

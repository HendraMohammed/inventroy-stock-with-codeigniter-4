<?php

namespace App\Controllers;

use App\Models\HardwareModel;

class Hardware extends BaseController
{

    protected $hardware;

    function __construct()
    {
        $this->hardware = new HardwareModel();
    }

    public function index()
    {
        $data['hardware'] = $this->hardware->findAll();
        return view('/hardware/index', $data);
    }

    public function create()
    {
        return view('hardware/create');
    }

    public function save()
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'jumlah' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'status' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $this->hardware->insert([
            'nama' => $this->request->getVar('nama'),
            'jumlah' => $this->request->getVar('jumlah'),
            'status' => $this->request->getVar('status'),
        ]);
        session()->setFlashdata('message', 'Tambah Data Hardware Berhasil');
        return redirect()->to('/hardware');
    }

    public function edit($id)
    {
        $dataHardware = $this->hardware->find($id);
        if (empty($dataHardware)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Hardware tidak ditemukan !');
        }
        $data['hardware'] = $dataHardware;
        return view('hardware/edit', $data);
    }

    public function update($id)
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'jumlah' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'status' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back();
        }

        $this->hardware->update($id, [
            'nama' => $this->request->getVar('nama'),
            'jumlah' => $this->request->getVar('jumlah'),
            'status' => $this->request->getVar('status'),
        ]);
        session()->setFlashdata('message', 'Update Data Hardware berhasil');
        return redirect()->to('/hardware');
    }

    public function delete($id)
    {
        $this->hardware->delete($id);
        return redirect()->to('/hardware');
    }
}

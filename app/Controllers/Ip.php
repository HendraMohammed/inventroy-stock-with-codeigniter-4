<?php

namespace App\Controllers;

use App\Models\IPModel;

class Ip extends BaseController
{
    protected $ip;

    function __construct()
    {
        $this->ip = new IpModel();
    }

    public function index()
    {
        $data['ip'] = $this->ip->findAll();
        return view('/ip/index', $data);
    }

    public function create()
    {
        return view('ip/create');
    }

    public function save()
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'jumlah' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'status' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'alamat_ip' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $this->ip->insert([
            'nama' => $this->request->getVar('nama'),
            'jumlah' => $this->request->getVar('jumlah'),
            'status' => $this->request->getVar('status'),
            'alamat_ip' => $this->request->getVar('alamat_ip'),
        ]);
        session()->setFlashdata('message', 'Tambah Data ip Berhasil');
        return redirect()->to('/ip');
    }

    public function edit($id)
    {
        $dataIp = $this->ip->find($id);
        if (empty($dataIp)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data ip tidak ditemukan !');
        }
        $data['ip'] = $dataIp;
        return view('ip/edit', $data);
    }

    public function update($id)
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'jumlah' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'status' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
            'alamat_ip' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back();
        }

        $this->ip->update($id, [
            'nama' => $this->request->getVar('nama'),
            'jumlah' => $this->request->getVar('jumlah'),
            'status' => $this->request->getVar('status'),
            'alamat_ip' => $this->request->getVar('alamat_ip'),
        ]);
        session()->setFlashdata('message', 'Update Data ip berhasil');
        return redirect()->to('/ip');
    }

    public function delete($id)
    {
        $this->ip->delete($id);
        return redirect()->to('/ip');
    }
}

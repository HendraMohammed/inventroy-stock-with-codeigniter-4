<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Huma extends BaseController
{
    public function index()
    {
        return view('huma');
    }

    public function template()
    {
        return view('layout/template');
    }
}

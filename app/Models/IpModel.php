<?php

namespace App\Models;

use CodeIgniter\Model;

class IPModel extends Model
{

    protected $table                = 'ip';
    protected $primaryKey           = 'id_ip';
    protected $returnType           = 'object';
    protected $useTimestamps        = true;
    protected $allowedFields        = ['id_ip', 'nama', 'jumlah', 'status', 'alamat_ip'];
}

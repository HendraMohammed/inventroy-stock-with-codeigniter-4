<?php

namespace App\Models;

use CodeIgniter\Model;

class HardwareModel extends Model
{
    protected $table                = 'hardware';
    protected $primaryKey           = 'id_hardware';
    protected $returnType           = 'object';
    protected $useTimestamps        = true;
    protected $allowedFields        = ['id_hardware', 'nama', 'jumlah', 'status'];
}

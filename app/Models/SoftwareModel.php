<?php

namespace App\Models;

use CodeIgniter\Model;

class SoftwareModel extends Model
{

    protected $table                = 'software';
    protected $primaryKey           = 'id_software';
    protected $returnType           = 'object';
    protected $useTimestamps        = true;
    protected $allowedFields        = ['id_software', 'nama', 'jumlah', 'status'];
}

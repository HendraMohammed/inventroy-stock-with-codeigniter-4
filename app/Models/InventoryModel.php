<?php

namespace App\Models;

use CodeIgniter\Model;

class InventoryModel extends Model
{
    protected $table                = 'inventory';
    protected $primaryKey           = 'id_inventory';
    protected $returnType           = 'object';
    protected $useTimestamps        = true;
    protected $allowedFields        = ['id_inventory', 'nama', 'jumlah', 'status'];
}

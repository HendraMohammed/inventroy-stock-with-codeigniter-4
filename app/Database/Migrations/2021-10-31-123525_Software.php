<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Software extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_software'   => [
                'type'  => 'INT',
                'constraint'    => '11',
                'unsigned'  => TRUE,
                'auto_increment'    => TRUE
            ],
            'nama'  => [
                'type'  => 'VARCHAR',
                'constraint'    => '255'
            ],
            'jumlah'  => [
                'type'  => 'VARCHAR',
                'constraint'    => '255'
            ],
            'status'  => [
                'type'  => 'VARCHAR',
                'constraint'    => '255'
            ],
            'created_at'  => [
                'type'  => 'DATETIME',
                'null'    => TRUE
            ],
            'updated_at'  => [
                'type'  => 'DATETIME',
                'null'    => TRUE
            ]
        ]);
        $this->forge->addPrimaryKey('id_software');
        $this->forge->createTable('software');
    }

    public function down()
    {
        $this->forge->dropTable('hardware');
    }
}

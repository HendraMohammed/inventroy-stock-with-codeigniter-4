<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Ip extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id_ip'   => [
                'type'  => 'INT',
                'constraint'    => '11',
                'unsigned'  => TRUE,
                'auto_increment'    => TRUE
            ],
            'nama'  => [
                'type'  => 'VARCHAR',
                'constraint'    => '255'
            ],
            'jumlah'  => [
                'type'  => 'VARCHAR',
                'constraint'    => '255'
            ],
            'status'  => [
                'type'  => 'VARCHAR',
                'constraint'    => '255'
            ],
            'alamat_ip'  => [
                'type'  => 'VARCHAR',
                'constraint'    => '255'
            ],
            'created_at'  => [
                'type'  => 'DATETIME',
                'null'    => TRUE
            ],
            'updated_at'  => [
                'type'  => 'DATETIME',
                'null'    => TRUE
            ]
        ]);
        $this->forge->addPrimaryKey('id_ip');
        $this->forge->createTable('ip');
    }

    public function down()
    {
        $this->forge->dropTable('ip');
    }
}

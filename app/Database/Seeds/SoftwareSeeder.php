<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class SoftwareSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama'          =>  'Office 2019',
                'jumlah' =>  '1',
                'status'       =>  'berlisensi',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'Cisco Packet Tracer',
                'jumlah' =>  '2',
                'status'       =>  'tidak berlisensi',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'Adobe after effect',
                'jumlah' =>  '3',
                'status'       =>  'berlisensi',
                'created_at' => Time::now()
            ]
        ];
        $this->db->table('software')->insertBatch($data);
    }
}

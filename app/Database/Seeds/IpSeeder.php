<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class IpSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama'          =>  'Kadiv Humas',
                'jumlah' =>  '1',
                'status'       =>  'terdaftar',
                'alamat_ip' => '192.168.1.12',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'Administrasi Bupati',
                'jumlah' =>  '1',
                'status'       =>  'tidak terdaftar',
                'alamat_ip' => '192.168.1.77',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'Kabag PolPP',
                'jumlah' =>  '1',
                'status'       =>  'terdaftar',
                'alamat_ip' => '192.168.1.34',
                'created_at' => Time::now()
            ]
        ];
        $this->db->table('ip')->insertBatch($data);
    }
}

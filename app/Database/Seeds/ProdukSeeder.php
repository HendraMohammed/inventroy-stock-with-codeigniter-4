<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class ProdukSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama'          =>  'PERDA NO 1 2021',
                'jumlah' =>  '1',
                'status'       =>  'berlaku',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'PERBUP NO 8 2018',
                'jumlah' =>  '2',
                'status'       =>  'dicabut',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'PERWALI NO 2 2021',
                'jumlah' =>  '3',
                'status'       =>  'berlaku',
                'created_at' => Time::now()
            ]
        ];
        $this->db->table('produk')->insertBatch($data);
    }
}

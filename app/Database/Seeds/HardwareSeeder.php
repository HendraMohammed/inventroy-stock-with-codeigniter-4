<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class HardwareSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama'          =>  'Monitor',
                'jumlah' =>  '1',
                'status'       =>  'baru',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'Mouse',
                'jumlah' =>  '2',
                'status'       =>  'baru',
                'created_at' => Time::now()
            ],
            [
                'nama'          =>  'Keyboard',
                'jumlah' =>  '3',
                'status'       =>  'baru',
                'created_at' => Time::now()
            ]
        ];
        $this->db->table('hardware')->insertBatch($data);
    }
}

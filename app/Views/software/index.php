<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Data Software</h3>
        </div>
        <div class="card-body">
            <?php if (!empty(session()->getFlashdata('message'))) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('message'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <a href="<?= base_url('/software/create'); ?>" class="btn btn-primary">+ Tambah</a>
            <hr />
            <table class="table table-bordered table-dark">
                <tr class="table-active">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jumlah (perCopy)</th>
                    <th>status</th>
                    <th>Action</th>
                </tr>
                <?php
                $no = 1;
                foreach ($software as $row) {
                ?>
                    <tr>
                        <td>IS10<?= $no++; ?></td>
                        <td><?= $row->nama; ?></td>
                        <td><?= $row->jumlah; ?></td>
                        <td><?= $row->status; ?></td>
                        <td>
                            <a title="Edit" href="<?= base_url("software/edit/$row->id_software"); ?>" class="btn btn-info">Edit</a>
                            <a title="Delete" href="<?= base_url("software/delete/$row->id_software") ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ?')">Delete</a>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>
<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Data Inventory</h3>
        </div>
        <div class="card-body">
            <hr />
            <table class="table table-bordered table-dark">
                <tr class="table-active">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>status</th>
                </tr>
                <?php
                $no = 1;
                foreach ($hardware as $row) {
                ?>
                    <tr>
                        <td>IH10<?= $no++; ?></td>
                        <td><?= $row->nama; ?></td>
                        <td><?= $row->jumlah; ?></td>
                        <td><?= $row->status; ?></td>
                    </tr>
                <?php
                }
                ?>
                <?php
                $no = 1;
                foreach ($software as $row) {
                ?>
                    <tr>
                        <td>IS10<?= $no++; ?></td>
                        <td><?= $row->nama; ?></td>
                        <td><?= $row->jumlah; ?></td>
                        <td><?= $row->status; ?></td>
                    </tr>
                <?php
                }
                ?>
                <?php
                $no = 1;
                foreach ($produk as $row) {
                ?>
                    <tr>
                        <td>IPH10<?= $no++; ?></td>
                        <td><?= $row->nama; ?></td>
                        <td><?= $row->jumlah; ?></td>
                        <td><?= $row->status; ?></td>
                    </tr>
                <?php
                }
                ?>
                <?php
                $no = 1;
                foreach ($ip as $row) {
                ?>
                    <tr>
                        <td>IPL10<?= $no++; ?></td>
                        <td><?= $row->nama; ?></td>
                        <td><?= $row->jumlah; ?></td>
                        <td><?= $row->status; ?></td>
                    </tr>
                <?php
                }
                ?>
            </table>
            <tr>
                <td>Keterangan :</td><br>
                <td>- Kode IH untuk item Hardware</td><br>
                <td>- Kode IS untuk item Software</td><br>
                <td>- Kode IPH untuk item Produk Hukum</td><br>
                <td>- Kode IPL untuk item IP List</td><br>
            </tr>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>
<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Data List IP</h3>
        </div>
        <div class="card-body">
            <?php if (!empty(session()->getFlashdata('message'))) : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('message'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <a href="<?= base_url('/ip/create'); ?>" class="btn btn-primary">+ Tambah</a>
            <hr />
            <table class="table table-bordered table-dark">
                <tr class="table-active">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>status</th>
                    <th>Alamat IP</th>
                    <th>Action</th>
                </tr>
                <?php
                $no = 1;
                foreach ($ip as $row) {
                ?>
                    <tr>
                        <td>IPL10<?= $no++; ?></td>
                        <td><?= $row->nama; ?></td>
                        <td><?= $row->jumlah; ?></td>
                        <td><?= $row->status; ?></td>
                        <td><?= $row->alamat_ip; ?></td>
                        <td>
                            <a title="Edit" href="<?= base_url("ip/edit/$row->id_ip"); ?>" class="btn btn-info">Edit</a>
                            <a title="Delete" href="<?= base_url("ip/delete/$row->id_ip") ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ?')">Delete</a>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>
<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="card">
    <div class="card-header">
        <h3>Ubah Alamat IP</h3>
    </div>
    <div class="card-body">
        <?php if (!empty(session()->getFlashdata('message'))) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('message'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <form method="post" action="<?= base_url("ip/update/$ip->id_ip"); ?>">
            <?= csrf_field(); ?>
            <div class="mb-3">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="<?= $ip->nama; ?>">
            </div>
            <div class="mb-3">
                <label for="jumlah" class="form-label">Jumlah</label>
                <input type="text" class="form-control" id="jumlah" name="jumlah" value="<?= $ip->jumlah; ?>">
            </div>
            <div class="mb-3">
                <label for="status" class="form-label">Status</label>
                <input type="text" class="form-control" id="status" name="status" value="<?= $ip->status; ?>">
            </div>
            <div class="mb-3">
                <label for="alamat_ip" class="form-label">Alamat IP</label>
                <input type="text" class="form-control" id="alamat_ip" name="alamat_ip" value="<?= $ip->alamat_ip; ?>">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <a title="kembali" href="<?= base_url("/hardware/index"); ?>" class="btn btn-outline-secondary" type="button">Kembali</a>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>
<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/register', 'Register::index');
$routes->post('/register/process', 'Register::process');
$routes->get('login', 'Login::index');
$routes->post('/login/process', 'Login::process');
$routes->get('/logout', 'Login::logout');
$routes->get('/hardware', 'Hardware::index');
$routes->get('/hardware/create', 'Hardware::create');
$routes->post('/hardware/save', 'Hardware::save');
$routes->get('/hardware/edit/(:num)', 'Hardware::edit/$1');
$routes->post('/hardware/update/(:num)', 'Hardware::update/$1');
$routes->get('/hardware/delete/(:num)', 'Hardware::delete/$1');
$routes->get('/software', 'Software::index');
$routes->get('/software/create', 'Software::create');
$routes->post('/software/save', 'Software::save');
$routes->get('/software/edit/(:num)', 'Software::edit/$1');
$routes->post('/software/update/(:num)', 'Software::update/$1');
$routes->get('/software/delete/(:num)', 'Software::delete/$1');
$routes->get('/produk', 'Produk::index');
$routes->get('/produk/create', 'Produk::create');
$routes->post('/produk/save', 'Produk::save');
$routes->get('/produk/edit/(:num)', 'Produk::edit/$1');
$routes->post('/produk/update/(:num)', 'Produk::update/$1');
$routes->get('/produk/delete/(:num)', 'Produk::delete/$1');
$routes->get('/ip', 'Ip::index');
$routes->get('/ip/create', 'Ip::create');
$routes->post('/ip/save', 'Ip::save');
$routes->get('/ip/edit/(:num)', 'Ip::edit/$1');
$routes->post('/ip/update/(:num)', 'Ip::update/$1');
$routes->get('/ip/delete/(:num)', 'Ip::delete/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

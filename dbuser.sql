-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2021 at 03:22 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbuser`
--

-- --------------------------------------------------------

--
-- Table structure for table `hardware`
--

CREATE TABLE `hardware` (
  `id_hardware` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hardware`
--

INSERT INTO `hardware` (`id_hardware`, `nama`, `jumlah`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Monitor', '1', 'baru', '2021-10-29 19:52:16', NULL),
(2, 'Mouse', '2', 'baru', '2021-10-29 19:52:16', NULL),
(3, 'Keyboard', '3', 'baru', '2021-10-29 19:52:16', NULL),
(4, 'printer', '5', 'baru', '2021-10-29 22:07:42', '2021-10-29 22:47:13'),
(6, 'joystik', '4', 'baru', '2021-10-31 08:15:35', '2021-10-31 08:15:48');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id_inventory` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ip`
--

CREATE TABLE `ip` (
  `id_ip` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `alamat_ip` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ip`
--

INSERT INTO `ip` (`id_ip`, `nama`, `jumlah`, `status`, `alamat_ip`, `created_at`, `updated_at`) VALUES
(1, 'Kadiv Humas', '1', 'terdaftar', '192.168.1.12', '2021-10-31 22:01:26', NULL),
(2, 'Administrasi Bupati', '1', 'tidak terdaftar', '192.168.1.77', '2021-10-31 22:01:26', NULL),
(3, 'Kabag PolPP', '1', 'terdaftar', '172.16.3.113', '2021-10-31 22:01:26', '2021-10-31 22:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2021-10-29-013407', 'App\\Database\\Migrations\\Users', 'default', 'App', 1635471939, 1),
(2, '2021-10-30-003423', 'App\\Database\\Migrations\\Hardware', 'default', 'App', 1635554654, 2),
(3, '2021-10-31-123525', 'App\\Database\\Migrations\\Software', 'default', 'App', 1635683954, 3),
(4, '2021-10-31-134628', 'App\\Database\\Migrations\\Produk', 'default', 'App', 1635688127, 4),
(5, '2021-11-01-024749', 'App\\Database\\Migrations\\Ip', 'default', 'App', 1635735042, 5),
(6, '2021-11-01-025435', 'App\\Database\\Migrations\\Ip', 'default', 'App', 1635735418, 6),
(7, '2021-11-01-215036', 'App\\Database\\Migrations\\Inventory', 'default', 'App', 1635803678, 7);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama`, `jumlah`, `status`, `created_at`, `updated_at`) VALUES
(1, 'PERDA NO 1 2021', '1', 'berlaku', '2021-10-31 09:04:02', NULL),
(2, 'PERBUP NO 8 2018', '2', 'dicabut', '2021-10-31 09:04:02', NULL),
(3, 'PERWALI NO 2 2021', '3', 'berlaku', '2021-10-31 09:04:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `software`
--

CREATE TABLE `software` (
  `id_software` int(11) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `software`
--

INSERT INTO `software` (`id_software`, `nama`, `jumlah`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Office 2019', '1', 'berlisensi', '2021-10-31 07:55:02', NULL),
(2, 'Cisco Packet Tracer', '2', 'tidak berlisensi', '2021-10-31 07:55:02', NULL),
(4, 'adobe premiere pro', '1', 'berlisensi', '2021-10-31 08:26:42', '2021-10-31 08:34:54'),
(5, 'microsoft frontpage', '1', 'tidak berlisensi', '2021-10-31 08:35:35', '2021-10-31 08:35:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `name`, `created_at`, `updated_at`) VALUES
('admin', '$2y$10$io6yorCU6bU/.lzdWsOjIO226oFuy.fy25Fxl9UHXbNiAZYkYUk.u', 'Mohammad Hendra Ferdansyah', '2021-11-01 21:16:24', '2021-11-01 21:16:24'),
('bismillah', '$2y$10$Pt.1u8eGmCgnYzobUTPLjuNva2tAWTHMOAaQ3AOEs1Y7oHpUAALqK', 'barokah syekali', '2021-10-28 21:38:40', '2021-10-28 21:38:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hardware`
--
ALTER TABLE `hardware`
  ADD PRIMARY KEY (`id_hardware`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id_inventory`);

--
-- Indexes for table `ip`
--
ALTER TABLE `ip`
  ADD PRIMARY KEY (`id_ip`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `software`
--
ALTER TABLE `software`
  ADD PRIMARY KEY (`id_software`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hardware`
--
ALTER TABLE `hardware`
  MODIFY `id_hardware` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id_inventory` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip`
--
ALTER TABLE `ip`
  MODIFY `id_ip` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `software`
--
ALTER TABLE `software`
  MODIFY `id_software` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
